#include <stdio.h>
#include <stdlib.h>
#include "linkedlist.h"

void printList(List* list) {
	Node* node;

  // Handle an empty node. Just print a message.
	if(list->head == NULL) {
		printf("\nEmpty List");
		return;
	}
	
  // Start with the head.
	node = (Node*) list->head;

	printf("\nList: \n\n\t"); 
	while(node != NULL) {
		//printf("[ %x ]", node->item);

    // Move to the next node
		node = (Node*) node->next;

		if(node !=NULL) {
			printf("-->");
    }
	}
	printf("\n\n");

	void initList(List* list_pointer);

// Create node containing item, return reference of it.
Node* createNode(void* item){
	typedef struct {
		void* item;
		struct Node*  next;
	}Node;
	
}

// Insert new item at the end of list.
void insertAtTail(List* list_pointer, void* item){
	Node* node;
	node->item = item;
	node->next = NULL;
	list_pointer->tail->next = NULL;
	list_pointer->tail = node;
}

// Insert item at start of the list.
void insertAtHead(List* list_pointer, void* item){
	Node* node;
	node->item = item;
	node->next = list_pointer->head->item;
	list_pointer->head = node;
}

// Insert item at a specified index.
void insertAtIndex(List* list_pointer, int index, void* item){
	Node* node;
	node = list_pointer->head;
	Node* prev;
	
	for (int i = 1; i < index; i++){
		prev = node;
		node->item = node->next;
	}
	Node* new_node;
	new_node->item =  item;
	new_node->next = node->item;
	prev->next = new_node->item;
}

// Remove item from the end of list and return a reference to it
void* removeTail(List* list_pointer){
	Node* node;
	node = list_pointer->tail;

	Node* new_node;
	new_node = list_pointer->head;

	while (new_node->next != list_pointer->tail->item){
		new_node->item = new_node->next;
	}
	new_node->next = NULL;
	list_pointer->tail = new_node;
	
	return list_pointer->tail;
}

// Remove item from start of list and return a reference to it
void* removeHead(List* list_pointer){
	Node* node;
	node = list_pointer->head;

	Node* next_node;
	next_node->item = list_pointer->head->next;
	list_pointer->head = next_node;

	return node;
}

// Insert item at a specified index and return a reference to it
void* removeAtIndex(List* list_pointer, int index){
	Node* node;
	node->item = list_pointer->head->item;
	node->next = list_pointer->head->next;

	for (int i = 1; i < index; i++){
		node->item = node->next;
	}
	Node* temp;
	temp->item = node->next;
	node->next = temp->next;
	return temp;

}

// Return item at index
void* itemAtIndex(List* list_pointer, int index){
	Node* node;
	node->item = list_pointer->head->item;
	node->next = list_pointer->head->next;

	for (int i = 1; i < index; i++){
		node->item = node->next;
	}
	return node;
}
}